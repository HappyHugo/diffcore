import dom from "./dom"

export default class diff {

    constructor( node, vnode, context ) {

        console.time( "diff" )

        this.dom = new dom()
        this.context = context

        var el = this.diffPass( node, vnode, false )

        console.timeEnd( "diff" )

        return el

    }


    /**
     * @method diffPass
     * @param { null || HTMLElement } root
     * @param { VirtualNode } vnode
     * @param { boolean } isSvg
     * @returns { HTMLElement }
     * @description Renders a virtual dom tree to an actual dom tree, and only applies the diffrence between the two.
     */

    diffPass( root, vnode, isSvg ) {

        var component = void 0

        // Will go step by step through the diff algorithm.

        // 1. Check if vnode is a Component
        if ( typeof vnode.nodeName !== "string" ) {

            // Vnode name is not a string, so we assume it's a component.

            component = vnode.nodeName
            component.props = vnode.attributes
            component.context = this.context

            vnode = component.render()

        } else {

            // Do nothing

        }

        // 2. Check if the nodeName is "svg"

        if ( vnode.nodeName === "svg" ) {

            // Set isSvg to true
            isSvg = true;

        } else {

            // we do nothing

            if ( isSvg ) {

                // "vnode.nodeName" is not "svg", but isSvg is true,
                // so this vnode is a child of a svg node and needs
                // the svg namespace to work properly.
                // currently we do nothing

            }

        }

        // 3. Diff node
        // console.log( "Diff node", root.nodeName, vnode.nodeName )

        if ( root === null ) {

            // Root dosen't exist, so we mount vnode

            root = this.dom.createElement( vnode.nodeName, isSvg )

            if ( typeof component !== "undefined" ) {

                // if component is set we set root as a refrence to that component

                component.refrence = root;

            }

        } else if ( root.nodeName !== vnode.nodeName.toUpperCase() ) {

            // "root.localName" is obsolete, use "root.nodeName" instead.
            // "root.nodeName" gives name in uppercase, 
            // so we need to uppercase "vnode.nodeName".

            // If "root" and vnode doesn't match we
            // simply delete "root" and mount "vnode"
            // after mount we can return "_new" since "root" is now unmounted

            var _parent = root.parentNode
            var _new = this.diffPass( null, vnode, isSvg )

            _parent.replaceChild( _new, root )

            return _new

        } else {

            // Nothing have changed, so do nothing

        }

        // 4. Diff attributes
        // console.log( "Diff attributes", root.attributes, vnode.attributes )

        var _appliedAttributes = new Map()
        // Loop over every virtual attribute
        for ( var attr in vnode.attributes ) {

            // check if node have any special attributes:
            //  "style" or "events"

            if ( attr === "style" ) {

                // Node style attribute

                root.style.cssText = ""
                for ( var _style in vnode.attributes[ attr ] ) {
                    root.style[ _style ] = vnode.attributes.style[ _style ]
                }

            } else if ( attr[ 0 ] === "o" && attr[ 1 ] === "n" ) {

                // Node event attribute
                // Every event start with "on"

                if ( typeof root.__widgetAttributes__ !== "undefined" ) {

                    if ( root.__widgetAttributes__.has( attr ) ) {

                        root.removeEventListener( attr.substring( 2 ), root.__widgetAttributes__.get( attr ) )

                    }

                    root.addEventListener( attr.substring( 2 ), vnode.attributes[ attr ] )


                } else {

                    root.addEventListener( attr.substring( 2 ), vnode.attributes[ attr ] )

                }


            } else {

                // Node attribute
                root.setAttribute( attr, vnode.attributes[ attr ] )

            }

            _appliedAttributes.set( attr, vnode.attributes[ attr ] )
            if ( typeof root.__widgetAttributes__ !== "undefined" ) {

                // remove updated attributes from hitlist
                root.__widgetAttributes__.delete( attr )

            }

        }

        // 5. Remove old attributes
        if ( typeof root.__widgetAttributes__ !== "undefined" ) {

            root.__widgetAttributes__.forEach( function ( _oldValue, _oldAttr ) {

                if ( _oldAttr[ 0 ] === "o" && _oldAttr[ 1 ] === "n" ) {
                    root.removeEventListener( _oldAttr.substring( 2 ), _oldValue )
                } else {
                    root.removeAttribute( _oldAttr )
                }

            } )

        }

        root.__widgetAttributes__ = _appliedAttributes;


        // 6. Diff elements
        // console.log( "Diff elements", root.childNodes, vnode.elements )

        var vChildren = vnode.elements;
        var dChildren = root.childNodes;

        if ( typeof vChildren === "undefined" || vChildren.length === 0 ) {

            // No virtual children so remove the real ones

            var next
            while ( next = root.lastChild ) {
                root.removeChild( next )
            }

        } else {

            var count = void 0

            vChildren.forEach( function ( child, index ) {

                count = index

                if ( typeof child === "string" ) {

                    // Child is text node
                    // console.log( "TEXTNODE", child )

                    if ( typeof dChildren[ index ] === "undefined" ) {

                        // Mount text node

                        root.appendChild( this.dom.createTextNode( child ) )

                    } else {

                        //diff text node
                        if ( typeof dChildren[ index ].splitText !== "undefined" ) {

                            if ( dChildren[ index ].nodeValue !== child ) {
                                dChildren[ index ].nodeValue = child;
                            }

                        } else {

                            node.replaceChild( this.dom.createTextNode( vChild ), dChildren[ index ] )

                        }

                    }

                } else {

                    // Child is a normal node
                    // We can now recursivly call interalDiff with "child"

                    // console.log( dChildren[ index ], child )

                    if ( typeof dChildren[ index ] === "undefined" ) {

                        // Mount virutal nodes
                        root.appendChild( this.diffPass( null, child, isSvg ) )

                    } else {

                        this.diffPass( dChildren[ index ], child, isSvg )

                    }

                }

            }.bind( this ) )

            // Remove unwanted children
            if ( dChildren.length - 1 !== count ) {

                for ( var i = dChildren.length - 1; i >= count + 1; i-- ) {

                    root.removeChild( dChildren[ i ] )

                }


            }

        }

        if ( typeof component !== "undefined" ) {

            if ( typeof component.componentAfterMount !== "undefined" ) {

                // We trigger the after mount lifecycle hook if it exists

                component.componentAfterMount.bind( component )();

            }
        }

        // 7. Finally return root
        return root;
    }

}