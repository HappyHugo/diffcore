export default function VirtualNode( nodeName, attributes, elements ) {

    if ( typeof nodeName === "function" ) {

        var component = new nodeName()

        this.nodeName = component;

    } else {

        this.nodeName = nodeName;
    
    }

    this.attributes = attributes;
    this.elements = elements;

}