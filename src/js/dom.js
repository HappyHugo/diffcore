export default class dom {

    /** @method */
    createElement( nodeName, isSvg ) {

        if ( isSvg ) {
            return document.createElementNS( "http://www.w3.org/2000/svg", nodeName )
        } else {
            return document.createElement( nodeName )
        }
    }

    /** @method */
    createTextNode( string ) {

        return document.createTextNode( string )

    }

}