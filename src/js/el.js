import VirtualNode from "./VirtualNode";

export default function el( nodeName, attributes, ...elements ) {

    return new VirtualNode( nodeName, attributes, elements )
   
}