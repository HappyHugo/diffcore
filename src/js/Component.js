import diff from "./diff"

export default class Component {

    constructor() {

        this.state = {}

        if ( typeof this.render === "undefined" ||
            typeof this.render !== "function" ) {
            throw new Error( "Missing a render function or not of type <<function>>" )
        }

    }

    setState( state ) {

        if ( typeof state !== "object" ) {
            throw new Error( "this.setState() needs an argument with type <<objcet>>" )
        }

        // Merge states
        this.state = Object.assign( this.state, state );

        if ( typeof this.componentBeforeUpdate !== "undefined" ) {

            // Lifecycle hook before update
            this.componentBeforeUpdate()

        }

        new diff( this.refrence, this.render(), this.context )

        if ( typeof this.componentAfterUpdate !== "undefined" ) {

            // Lifecycle hook after update
            this.componentAfterUpdate()

        }

    }

}